//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	c = 0;
	z = 0;
	Player = 0;
	tc->First();
	tc->TabPosition = TTabPosition::None;
	DoLevel();
	ChangePlayer(Player);
	p1 = 0;
	p2 = 0;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buStartClick(TObject *Sender)
{
	tc->ActiveTab = tiGame;
    DoLevel();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buMenuClick(TObject *Sender)
{
	tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------
void Tfm::ChangePlayer(bool Player)
{
	if (Player == 0){
		laMove->Text = (L"��� ������ 1");
		rc->Fill->Color = (claBrown);
	}
	else{
		laMove->Text = (L"��� ������ 2");
		rc->Fill->Color = (claMediumslateblue);
	}
}
//---------------------------------------------------------------------------
void Tfm::DoLevel()
{
	Stop = 0;
	bu1->Enabled = true;
	b1 = 0;
	imCross1->ImageIndex = -1;
	imZero1->ImageIndex = -1;
	bu2->Enabled = true;
	b2 = 0;
	imCross2->ImageIndex = -1;
	imZero2->ImageIndex = -1;
	bu3->Enabled = true;
	b3 = 0;
	imCross3->ImageIndex = -1;
	imZero3->ImageIndex = -1;
	bu4->Enabled = true;
	b4 = 0;
	imCross4->ImageIndex = -1;
	imZero4->ImageIndex = -1;
	bu5->Enabled = true;
	b5 = 0;
	imCross5->ImageIndex = -1;
	imZero5->ImageIndex = -1;
	bu6->Enabled = true;
	b6 = 0;
	imCross6->ImageIndex = -1;
	imZero6->ImageIndex = -1;
	bu7->Enabled = true;
	b7 = 0;
	imCross7->ImageIndex = -1;
	imZero7->ImageIndex = -1;
	bu8->Enabled = true;
	b8 = 0;
	imCross8->ImageIndex = -1;
	imZero8->ImageIndex = -1;
	bu9->Enabled = true;
	b9 = 0;
	imCross9->ImageIndex = -1;
	imZero9->ImageIndex = -1;
	Player = Random(2);
	ChangePlayer(Player);
	imCrossEnd->Visible = false;
	imZeroEnd->Visible = false;
}
//---------------------------------------------------------------------------
void Tfm::Click()
{
	(Player == 0) ? Player = 1: Player = 0;
	ChangePlayer(Player);
	CheckWin(Player);
}
//---------------------------------------------------------------------------
void Tfm::CheckWin(bool Player)
{
	if ((b1 == 1 && b2 == 1 && b3 == 1)  || (b4 == 1 && b5 == 1 && b6 == 1) || (b7 == 1 && b8 == 1 && b9 == 1) ||
	(b1 == 1 && b4 == 1 && b7 == 1)|| (b2 == 1 && b5 == 1 && b8 == 1)|| (b3 == 1 && b6 == 1 && b9 == 1)|| (b1 == 1 && b5 == 1 && b9 == 1)||
	(b3 == 1 && b5 == 1 && b7 == 1)){
		laWin->Text = (L"����� 1 �������");
		imCrossEnd->Visible = true;
		tc->ActiveTab = tiResult;
		p1++;
		Stop = 1;
		laP1Win->Text = Format(L"����� 1 ������� %d ���", ARRAYOFCONST((p1)));
	}
	if ((b1 == 2 && b2 == 2 && b3 == 2)  || (b4 == 2 && b5 == 2 && b6 == 2) || (b7 == 2 && b8 == 2 && b9 == 2) ||
	(b1 == 2 && b4 == 2 && b7 == 2)|| (b2 == 2 && b5 == 2 && b8 == 2)|| (b3 == 2 && b6 == 2 && b9 == 2)|| (b1 == 2 && b5 == 2 && b9 == 2)||
	(b3 == 2 && b5 == 2 && b7 == 2)){
		laWin->Text = (L"����� 2 �������");
		imZeroEnd->Visible = true;
		tc->ActiveTab = tiResult;
		p2++;
		Stop = 1;
		laP2Win->Text = Format(L"����� 2 ������� %d ���", ARRAYOFCONST((p2)));
	}
	if ((b1 == 1 || b1 == 2) && (b2 == 1 || b2 == 2) && (b3 == 1 || b3 == 2) &&
	(b4 == 1 || b4 == 2) && (b5 == 1 || b5 == 2) && (b6 == 1 || b6 == 2) &&
	(b7 == 1 || b7 == 2) && (b8 == 1 || b8 == 2) && (b9 == 1 || b9 == 2) && Stop == 0){
		laWin->Text = (L"�����!");
		tc->ActiveTab = tiResult;
	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::bu1Click(TObject *Sender)
{
	if (Player == 0) {
		if (c == 0) {
			imCross1->ImageIndex = 0;
		}
		if (c == 1) {
			imCross1->ImageIndex = 1;
		}
		if (c == 2) {
			imCross1->ImageIndex = 2;
		}
		b1 = 1;
	}
	else {
		if (z == 0) {
			imZero1->ImageIndex = 0;
		}
		if (z == 1) {
			imZero1->ImageIndex = 1;
		}
		if (z == 2) {
			imZero1->ImageIndex = 2;
		}
		b1 = 2;
	}
	bu1->Enabled = false;
	Click();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::bu2Click(TObject *Sender)
{
	if (Player == 0) {
		if (c == 0) {
			imCross2->ImageIndex = 0;
		}
		if (c == 1) {
			imCross2->ImageIndex = 1;
		}
		if (c == 2) {
			imCross2->ImageIndex = 2;
		}
		b2 = 1;
	}
	else {
		if (z == 0) {
			imZero2->ImageIndex = 0;
		}
		if (z == 1) {
			imZero2->ImageIndex = 1;
		}
		if (z == 2) {
			imZero2->ImageIndex = 2;
		}
		b2 = 2;
	}
	bu2->Enabled = false;
	Click();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::bu3Click(TObject *Sender)
{
	if (Player == 0) {
		if (c == 0) {
			imCross3->ImageIndex = 0;
		}
		if (c == 1) {
			imCross3->ImageIndex = 1;
		}
		if (c == 2) {
			imCross3->ImageIndex = 2;
		}
		b3 = 1;
	}
	else {
		if (z == 0) {
			imZero3->ImageIndex = 0;
		}
		if (z == 1) {
			imZero3->ImageIndex = 1;
		}
		if (z == 2) {
			imZero3->ImageIndex = 2;
		}
		b3 = 2;
	}
	bu3->Enabled = false;
	Click();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::bu4Click(TObject *Sender)
{
	if (Player == 0) {
		if (c == 0) {
			imCross4->ImageIndex = 0;
		}
		if (c == 1) {
			imCross4->ImageIndex = 1;
		}
		if (c == 2) {
			imCross4->ImageIndex = 2;
		}
		b4 = 1;
	}
	else {
		if (z == 0) {
			imZero4->ImageIndex = 0;
		}
		if (z == 1) {
			imZero4->ImageIndex = 1;
		}
		if (z == 2) {
			imZero4->ImageIndex = 2;
		}
		b4 = 2;
	}
	bu4->Enabled = false;
	Click();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::bu5Click(TObject *Sender)
{
	if (Player == 0) {
		if (c == 0) {
			imCross5->ImageIndex = 0;
		}
		if (c == 1) {
			imCross5->ImageIndex = 1;
		}
		if (c == 2) {
			imCross5->ImageIndex = 2;
		}
		b5 = 1;
	}
	else {
		if (z == 0) {
			imZero5->ImageIndex = 0;
		}
		if (z == 1) {
			imZero5->ImageIndex = 1;
		}
		if (z == 2) {
			imZero5->ImageIndex = 2;
		}
		b5 = 2;
	}
	bu5->Enabled = false;
	Click();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::bu6Click(TObject *Sender)
{
	if (Player == 0) {
		if (c == 0) {
			imCross6->ImageIndex = 0;
		}
		if (c == 1) {
			imCross6->ImageIndex = 1;
		}
		if (c == 2) {
			imCross6->ImageIndex = 2;
		}
		b6 = 1;
	}
	else {
		if (z == 0) {
			imZero6->ImageIndex = 0;
		}
		if (z == 1) {
			imZero6->ImageIndex = 1;
		}
		if (z == 2) {
			imZero6->ImageIndex = 2;
		}
		b6 = 2;
	}
	bu6->Enabled = false;
	Click();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::bu7Click(TObject *Sender)
{
	if (Player == 0) {
		if (c == 0) {
			imCross7->ImageIndex = 0;
		}
		if (c == 1) {
			imCross7->ImageIndex = 1;
		}
		if (c == 2) {
			imCross7->ImageIndex = 2;
		}
		b7 = 1;
	}
	else {
		if (z == 0) {
			imZero7->ImageIndex = 0;
		}
		if (z == 1) {
			imZero7->ImageIndex = 1;
		}
		if (z == 2) {
			imZero7->ImageIndex = 2;
		}
		b7 = 2;
	}
	bu7->Enabled = false;
	Click();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::bu8Click(TObject *Sender)
{
	if (Player == 0) {
		if (c == 0) {
			imCross8->ImageIndex = 0;
		}
		if (c == 1) {
			imCross8->ImageIndex = 1;
		}
		if (c == 2) {
			imCross8->ImageIndex = 2;
		}
		b8 = 1;
	}
	else {
		if (z == 0) {
			imZero8->ImageIndex = 0;
		}
		if (z == 1) {
			imZero8->ImageIndex = 1;
		}
		if (z == 2) {
			imZero8->ImageIndex = 2;
		}
		b8 = 2;
	}
	bu8->Enabled = false;
	Click();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::bu9Click(TObject *Sender)
{
	if (Player == 0) {
		if (c == 0) {
			imCross9->ImageIndex = 0;
		}
		if (c == 1) {
			imCross9->ImageIndex = 1;
		}
		if (c == 2) {
			imCross9->ImageIndex = 2;
		}
		b9 = 1;
	}
	else {
		if (z == 0) {
			imZero9->ImageIndex = 0;
		}
		if (z == 1) {
			imZero9->ImageIndex = 1;
		}
		if (z == 2) {
			imZero9->ImageIndex = 2;
		}
		b9 = 2;
	}
	bu9->Enabled = false;
	Click();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buRestartClick(TObject *Sender)
{
	if (tc->ActiveTab->Index == 1) {
		DoLevel();
	}
	else {
		tc->ActiveTab = tiGame;
		DoLevel();
	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buStatClick(TObject *Sender)
{
	tc->ActiveTab = tiStat;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buAboutClick(TObject *Sender)
{
	ShowMessage(L"������ �1 ���������� ������");
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buOptionsClick(TObject *Sender)
{
	tc->ActiveTab = tiOptions;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buSelectCrossClick(TObject *Sender)
{
	if (((TButton *)Sender)->Tag == 0) {
		c = 0;
		imSelectC0->Visible = true;
		imSelectC1->Visible = false;
		imSelectC2->Visible = false;
	}
	if (((TButton *) Sender)->Tag == 1) {
		c = 1;
		imSelectC0->Visible = false;
		imSelectC1->Visible = true;
		imSelectC2->Visible = false;
	}
	if (((TButton *) Sender)->Tag == 2) {
		c = 2;
		imSelectC0->Visible = false;
		imSelectC1->Visible = false;
		imSelectC2->Visible = true;
	}
}
//---------------------------------------------------------------------------



void __fastcall Tfm::buDefaultClick(TObject *Sender)
{
	c = 0;
	z = 0;
    imSelectC0->Visible = true;
	imSelectC1->Visible = false;
	imSelectC2->Visible = false;
	imSelectZ0->Visible = true;
	imSelectZ1->Visible = false;
	imSelectZ2->Visible = false;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buSelectZeroClick(TObject *Sender)
{
	if (((TButton *)Sender)->Tag == 0) {
		z = 0;
		imSelectZ0->Visible = true;
		imSelectZ1->Visible = false;
		imSelectZ2->Visible = false;
	}
	if (((TButton *) Sender)->Tag == 1) {
		z = 1;
		imSelectZ0->Visible = false;
		imSelectZ1->Visible = true;
		imSelectZ2->Visible = false;
	}
	if (((TButton *) Sender)->Tag == 2) {
		z = 2;
		imSelectZ0->Visible = false;
		imSelectZ1->Visible = false;
		imSelectZ2->Visible = true;
	}
}
//---------------------------------------------------------------------------

