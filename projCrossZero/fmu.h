//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.ImgList.hpp>
#include <System.ImageList.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *tb;
	TLabel *Label1;
	TButton *buAbout;
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiGame;
	TButton *buStart;
	TGridPanelLayout *GridPanelLayout1;
	TRectangle *rc;
	TLabel *laMove;
	TGridPanelLayout *GridPanelLayout2;
	TButton *buMenu;
	TButton *buRestart;
	TTabItem *tiResult;
	TGridPanelLayout *GridPanelLayout3;
	TButton *Button1;
	TButton *Button2;
	TLayout *Layout2;
	TButton *bu2;
	TLayout *Layout1;
	TButton *bu1;
	TLayout *Layout3;
	TButton *bu3;
	TLayout *Layout4;
	TButton *bu4;
	TLayout *Layout5;
	TButton *bu5;
	TLayout *Layout6;
	TButton *bu6;
	TLayout *Layout7;
	TButton *bu7;
	TLayout *Layout8;
	TButton *bu8;
	TLayout *Layout9;
	TButton *bu9;
	TLayout *Layout10;
	TLabel *laWin;
	TButton *buStat;
	TTabItem *tiStat;
	TGridPanelLayout *GridPanelLayout4;
	TLabel *laP1Win;
	TLabel *laP2Win;
	TButton *Button3;
	TStyleBook *StyleBook1;
	TGridPanelLayout *GridPanelLayout5;
	TImage *imCrossEnd;
	TImage *imZeroEnd;
	TImageList *ImageList1;
	TImageList *ImageList2;
	TButton *buOptions;
	TTabItem *tiOptions;
	TLayout *Layout11;
	TLayout *Layout12;
	TLabel *Label2;
	TLayout *Layout13;
	TGridPanelLayout *GridPanelLayout6;
	TButton *buSelectCross0;
	TButton *buSelectCross1;
	TButton *buSelectCross2;
	TImage *imSelectCross0;
	TImage *imSelectCross1;
	TImage *imSelectCross2;
	TImage *imSelectZero1;
	TImage *imSelectZero2;
	TImage *imSelectZero0;
	TGlyph *imCross1;
	TGlyph *imZero1;
	TGridPanelLayout *GridPanelLayout7;
	TButton *Button4;
	TButton *buDefault;
	TButton *buSelectZero0;
	TButton *buSelectZero1;
	TButton *buSelectZero2;
	TImage *imSelectC0;
	TImage *imSelectC1;
	TImage *imSelectC2;
	TImage *imSelectZ0;
	TImage *imSelectZ1;
	TImage *imSelectZ2;
	TGlyph *imCross2;
	TGlyph *imZero2;
	TGlyph *imCross3;
	TGlyph *imZero3;
	TGlyph *imCross4;
	TGlyph *imZero4;
	TGlyph *imCross5;
	TGlyph *imZero5;
	TGlyph *imCross6;
	TGlyph *imZero6;
	TGlyph *imCross7;
	TGlyph *imZero7;
	TGlyph *imCross8;
	TGlyph *imZero8;
	TGlyph *imCross9;
	TGlyph *imZero9;
	void __fastcall buStartClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buMenuClick(TObject *Sender);
	void __fastcall bu1Click(TObject *Sender);
	void __fastcall bu2Click(TObject *Sender);
	void __fastcall bu3Click(TObject *Sender);
	void __fastcall bu4Click(TObject *Sender);
	void __fastcall bu5Click(TObject *Sender);
	void __fastcall bu6Click(TObject *Sender);
	void __fastcall bu7Click(TObject *Sender);
	void __fastcall bu8Click(TObject *Sender);
	void __fastcall bu9Click(TObject *Sender);
	void __fastcall buRestartClick(TObject *Sender);
	void __fastcall buStatClick(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall buOptionsClick(TObject *Sender);
	void __fastcall buSelectCrossClick(TObject *Sender);
	void __fastcall buDefaultClick(TObject *Sender);
	void __fastcall buSelectZeroClick(TObject *Sender);
private:	// User declarations
	bool Player;
	bool Win;
	bool Stop;
	int c;
    int z;
	int b1;
	int b2;
	int b3;
	int b4;
	int b5;
	int b6;
	int b7;
	int b8;
	int b9;
	int p1;
    int p2;
	void ChangePlayer(bool Player);
	void CheckWin(bool Win);
	void DoLevel();
	void Click();
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
