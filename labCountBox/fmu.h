//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include "uUtils.h"
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiLevel;
	TTabItem *tiOptions;
	TTabItem *tiAbout;
	TTabItem *tiPlay;
	TTabItem *tiFinish;
	TLabel *laQuestion;
	TLayout *lyTop;
	TButton *buBack;
	TLabel *laTime;
	TGridPanelLayout *GridPanelLayout1;
	TGridPanelLayout *GridPanelLayout2;
	TButton *buAnswer1;
	TButton *buAnswer2;
	TButton *buAnswer3;
	TButton *buAnswer4;
	TButton *buAnswer5;
	TButton *buAnswer6;
	TLayout *Layout1;
	TRectangle *Rectangle1;
	TRectangle *Rectangle2;
	TRectangle *Rectangle3;
	TRectangle *Rectangle4;
	TRectangle *Rectangle5;
	TRectangle *Rectangle6;
	TRectangle *Rectangle7;
	TRectangle *Rectangle8;
	TRectangle *Rectangle9;
	TRectangle *Rectangle10;
	TRectangle *Rectangle11;
	TRectangle *Rectangle12;
	TRectangle *Rectangle13;
	TRectangle *Rectangle14;
	TRectangle *Rectangle15;
	TRectangle *Rectangle16;
	TRectangle *Rectangle17;
	TRectangle *Rectangle18;
	TRectangle *Rectangle19;
	TRectangle *Rectangle20;
	TRectangle *Rectangle21;
	TRectangle *Rectangle22;
	TRectangle *Rectangle23;
	TRectangle *Rectangle24;
	TRectangle *Rectangle25;
	TTimer *tmPlay;
	TLayout *Layout2;
	TLabel *Label1;
	TGridPanelLayout *GridPanelLayout3;
	TRectangle *Rectangle26;
	TLabel *laCorrect;
	TRectangle *Rectangle27;
	TLabel *laWrong;
	TLayout *Layout3;
	TGridPanelLayout *GridPanelLayout4;
	TButton *Button2;
	TButton *Button1;
	TButton *Button3;
	TGridPanelLayout *GridPanelLayout5;
	TButton *Button4;
	TButton *Button5;
	TButton *Button6;
	TLayout *Layout4;
	TLabel *Label2;
	TButton *Button7;
	TGridPanelLayout *GridPanelLayout6;
	TButton *buEasy;
	TButton *buNormal;
	TButton *buHard;
	TGridPanelLayout *GridPanelLayout7;
	TLayout *Layout5;
	TLabel *Label3;
	TButton *Button8;
	TRectangle *Rectangle28;
	TLayout *Layout6;
	TLabel *laColor;
	TRectangle *Rectangle29;
	TRectangle *Rectangle30;
	TRectangle *Rectangle31;
	TLabel *Label4;
	TLayout *Layout7;
	TLabel *Label5;
	TButton *Button9;
	TLayout *Layout8;
	TLabel *laResult;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall buAnswerAllClick(TObject *Sender);
	void __fastcall tmPlayTimer(TObject *Sender);
	void __fastcall buResetClick(TObject *Sender);
	void __fastcall GoToLevelClick(TObject *Sender);
	void __fastcall BackClick(TObject *Sender);
	void __fastcall GoToOptionsClick(TObject *Sender);
	void __fastcall GoToAboutClick(TObject *Sender);
	void __fastcall buEasyClick(TObject *Sender);
	void __fastcall buNormalClick(TObject *Sender);
	void __fastcall buHardClick(TObject *Sender);
	void __fastcall GoToMenuClick(TObject *Sender);
	void __fastcall ColorClick(TObject *Sender);
private:	// User declarations
	int Win = 15;
	int Lose = 5;
	int h = 1;
	int u;
	int FCountCorrect;
	int FCountWrong;
	int FNumberCorrect;
	double FTimeValue;
	TList *FListBox;
	TList *FListAnswer;
	void DoReset();
	void DoContinue();
	void DoAnswer(int aValue);
	void DoFinish();
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
const int cMaxBox = 25;
const int cMaxAnswer = 6;
int cMinPossible = 4;
int cMaxPossible = 14;
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
