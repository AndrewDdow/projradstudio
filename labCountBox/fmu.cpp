//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
    tc->TabPosition = TTabPosition::None;
	FListBox = new TList;
	for (int i = 1; i <= cMaxBox; i++) {
		FListBox->Add(this->FindComponent("Rectangle" + IntToStr(i)));
	}
	//
	FListAnswer = new TList;
	FListAnswer->Add(buAnswer1);
	FListAnswer->Add(buAnswer2);
	FListAnswer->Add(buAnswer3);
	FListAnswer->Add(buAnswer4);
	FListAnswer->Add(buAnswer5);
	FListAnswer->Add(buAnswer6);
	tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormDestroy(TObject *Sender)
{
	delete FListBox;
	delete FListAnswer;
}
//---------------------------------------------------------------------------
void Tfm::DoReset()
{
	if (u == 1) {
			FTimeValue = Time().Val + (double)30/(24*60*60); //30 sec
	}
	if (u == 2) {
			FTimeValue = Time().Val + (double)20/(24*60*60); //20 sec
	}
	if (u == 3) {
			FTimeValue = Time().Val + (double)10/(24*60*60); //10 sec
	}
	tc->ActiveTab = tiPlay;
	FCountCorrect = 0;
	FCountWrong = 0;

	tmPlay->Enabled = true;
	DoContinue();
}
//---------------------------------------------------------------------------
void Tfm::DoContinue()
{
	for (int i = 0; i < cMaxBox; i++) {
		((TRectangle*)FListBox->Items[i])->Fill->Color = TAlphaColorRec::Lightgray;
	}
	//
	FNumberCorrect = RandomRange(cMinPossible, cMaxPossible);
	int *x = RandomArrayUnique(cMaxBox, FNumberCorrect);
	if (h == 1) {
		for (int i = 0; i < FNumberCorrect; i++) {
		((TRectangle*)FListBox->Items[x[i]])->Fill->Color = TAlphaColorRec::Cornflowerblue;
		}
	}
	if (h == 2) {
		for (int i = 0; i < FNumberCorrect; i++) {
		((TRectangle*)FListBox->Items[x[i]])->Fill->Color = TAlphaColorRec::Crimson;
		}
	}
	if (h == 3) {
		for (int i = 0; i < FNumberCorrect; i++) {
		((TRectangle*)FListBox->Items[x[i]])->Fill->Color = TAlphaColorRec::Greenyellow;
		}
	}
	if (h == 4) {
		for (int i = 0; i < FNumberCorrect; i++) {
		((TRectangle*)FListBox->Items[x[i]])->Fill->Color = TAlphaColorRec::Orange;
		}
	}
	//
	int xAnswerStart = FNumberCorrect - Random(cMaxAnswer-1);
	if (xAnswerStart < cMinPossible) {
		xAnswerStart = cMinPossible;
	}
	//
	for (int i = 0; i < cMaxAnswer; i++) {
		((TButton*)FListAnswer->Items[i])->Text = IntToStr(xAnswerStart + i);
	}
}
//---------------------------------------------------------------------------
void Tfm::DoAnswer(int aValue)
{
	(aValue == FNumberCorrect) ? FCountCorrect++ : FCountWrong++;
	if (FCountWrong >= Lose || FCountCorrect >= Win) {
		//*TODO*//
		DoFinish();
	}
	DoContinue();
}
//---------------------------------------------------------------------------
void Tfm::DoFinish()
{
	tmPlay->Enabled = false;
	if (FCountCorrect >= Win) {
		laResult->Text = L"You win!";
	}
	if (FCountWrong >= Lose) {
		laResult->Text = L"You lose!";
	}
	if (FCountCorrect < Win || FCountWrong < Lose) {
		laResult->Text = L"Not bad!";
	}
	laCorrect->Text = Format(L"Correct = %d", ARRAYOFCONST((FCountCorrect)));
	laWrong->Text = Format(L"Wrong = %d", ARRAYOFCONST((FCountWrong)));
	tc->ActiveTab = tiFinish;

}
void __fastcall Tfm::buAnswerAllClick(TObject *Sender)
{
	DoAnswer(StrToInt(((TButton*)Sender)->Text));
}
//---------------------------------------------------------------------------


void __fastcall Tfm::tmPlayTimer(TObject *Sender)
{
	double x = FTimeValue - Time().Val;
	laTime->Text = FormatDateTime("nn:ss", x);
	if (x <= 0) {
		DoFinish();
	}
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buResetClick(TObject *Sender)
{
    DoReset();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::GoToLevelClick(TObject *Sender)
{
	tc->ActiveTab = tiLevel;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::BackClick(TObject *Sender)
{
	tmPlay->Enabled = false;
	tc->Previous();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::GoToOptionsClick(TObject *Sender)
{
	 tc->ActiveTab = tiOptions;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::GoToAboutClick(TObject *Sender)
{
	tc->ActiveTab = tiAbout;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buEasyClick(TObject *Sender)
{
	cMinPossible = 1;
	cMaxPossible = 8;
	u = 1;
	Win = 15;
	Lose = 6;
	DoReset();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buNormalClick(TObject *Sender)
{
	cMinPossible = 4;
	cMaxPossible = 14;
	u = 2;
	Win = 10;
	Lose = 4;
	DoReset();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buHardClick(TObject *Sender)
{
    cMinPossible = 1;
	cMaxPossible = 15;
	u = 3;
	Win = 5;
	Lose = 2;
	DoReset();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::GoToMenuClick(TObject *Sender)
{
	tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::ColorClick(TObject *Sender)
{
	if (((TRectangle*)Sender)->Fill->Color == TAlphaColorRec::Cornflowerblue){
		h = 1;
		laColor->Text = "Your color is blue";
		laQuestion->Text = "How many blue squares?";
	}
	if (((TRectangle*)Sender)->Fill->Color == TAlphaColorRec::Crimson){
		h = 2;
		laColor->Text = "Your color is red";
		laQuestion->Text = "How many red squares?";
	}
	if (((TRectangle*)Sender)->Fill->Color == TAlphaColorRec::Greenyellow){
		h = 3;
		laColor->Text = "Your color is green";
		laQuestion->Text = "How many green squares?";
	}
	if (((TRectangle*)Sender)->Fill->Color == TAlphaColorRec::Orange){
		h = 4;
		laColor->Text = "Your color is orange";
		laQuestion->Text = "How many orange squares?";

	}

}
//---------------------------------------------------------------------------

