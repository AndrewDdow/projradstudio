//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.ImgList.hpp>
#include <FMX.Layouts.hpp>
#include <System.ImageList.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiPlay;
	TGridPanelLayout *GridPanelLayout1;
	TImageList *il;
	TGlyph *Glyph1;
	TLayout *Layout1;
	TLayout *Layout2;
	TGlyph *Glyph2;
	TLayout *Layout3;
	TGlyph *Glyph3;
	TLayout *Layout4;
	TGlyph *Glyph4;
	TLayout *Layout5;
	TGlyph *Glyph5;
	TLayout *Layout6;
	TGlyph *Glyph6;
	TLayout *Layout7;
	TGlyph *Glyph7;
	TLayout *Layout8;
	TGlyph *Glyph8;
	TLayout *Layout9;
	TGlyph *Glyph9;
	TToolBar *ToolBar1;
	TLayout *Layout10;
	TButton *buRestart;
	TTabItem *tiMenu;
	TTabItem *tiLevel;
	TToolBar *ToolBar2;
	TLayout *Layout11;
	TGridPanelLayout *GridPanelLayout2;
	TButton *buStart;
	TButton *buStat;
	TLayout *Layout12;
	TGridPanelLayout *GridPanelLayout3;
	TButton *Button1;
	TButton *Button2;
	TToolBar *ToolBar3;
	TTabItem *tiStat;
	TLayout *Layout13;
	TGridPanelLayout *GridPanelLayout4;
	TButton *buBack;
	TLayout *Layout14;
	TLabel *Label1;
	TLabel *laWinEasy;
	TLayout *Layout16;
	TLabel *Label3;
	TLabel *laRestart;
	TLayout *Layout15;
	TLabel *Label2;
	TLabel *laWinHard;
	TToolBar *ToolBar4;
	TButton *Button4;
	TButton *Button3;
	TLabel *Label4;
	TLabel *Label5;
	TLabel *laLozung;
	TLabel *Label6;
	TLayout *Layout17;
	TLabel *Label7;
	TLabel *laTurn;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall LayoutAllClick(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
	void __fastcall buEasyClick(TObject *Sender);
	void __fastcall buHardClick(TObject *Sender);
	void __fastcall buStartClick(TObject *Sender);
	void __fastcall buRestartClick(TObject *Sender);
	void __fastcall buStatClick(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);

private:	// User declarations
	TGlyph *m[9];
	int x;
	int y;
	int level;
	int win;
	int field;
	int RestartCount;
	int WinCountEasy;
	int WinCountHard;
	int TurnCount;
    int TurnBestCount;
	bool once;
	bool LevelKey;
	void Start();
	void Easy();
	void Hard();
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
