//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	RestartCount = 0;
	WinCountEasy = 0;
	WinCountHard = 0;
	TurnCount = 0;
	TurnBestCount = 1000;
	field = 9;
	once = false;
	tc->TabPosition = TTabPosition::None;
	tc->ActiveTab = tiMenu;
	m[0] = Glyph1;
	m[1] = Glyph2;
	m[2] = Glyph3;
	m[3] = Glyph4;
	m[4] = Glyph5;
	m[5] = Glyph6;
	m[6] = Glyph7;
	m[7] = Glyph8;
	m[8] = Glyph9;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::LayoutAllClick(TObject *Sender)
{
		ShowMessage(((TLayout*)Sender)->Tag);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
		  TShiftState Shift)
{
	once = true;
	win = 0;
	TurnCount++;
	for (int i = 0; i < field; i++) {
		if (m[i]->ImageIndex == -1 && once == true) {
//			if (Key == vkLeft && m[i]->Parent->Tag != 3 && m[i]->Parent->Tag != 6 && m[i]->Parent->Tag != 9) {
//				x = m[i]->ImageIndex;
//				m[i]->ImageIndex = m[i+1]->ImageIndex;
//				m[i+1]->ImageIndex = x;
//				once = false;
//			}
			if (ssShift) {
				ShowMessage("Cool");
			}
			if (Key == vkRight && m[i]->Parent->Tag != 1 && m[i]->Parent->Tag != 4 && m[i]->Parent->Tag != 7) {
				y = m[i]->ImageIndex;
				m[i]->ImageIndex = m[i-1]->ImageIndex;
				m[i-1]->ImageIndex = y;
				once = false;
			}
			if (Key == vkUp && m[i]->Parent->Tag != 7 && m[i]->Parent->Tag != 8 && m[i]->Parent->Tag != 9) {
				y = m[i]->ImageIndex;
				m[i]->ImageIndex = m[i+3]->ImageIndex;
				m[i+3]->ImageIndex = y;
				once = false;
			}
			if (Key == vkDown && m[i]->Parent->Tag != 1 && m[i]->Parent->Tag != 2 && m[i]->Parent->Tag != 3) {
				y = m[i]->ImageIndex;
				m[i]->ImageIndex = m[i-3]->ImageIndex;
				m[i-3]->ImageIndex = y;
				once = false;
			}
		}
		if (m[i]->ImageIndex == i) {
			win++;
		}
		else {
			win = 0;
		}
		if (m[field-1]->ImageIndex == -1) {
			win++;
		}
		if (win == field) {
			ShowMessage(TurnCount);
			if (TurnCount < TurnBestCount) {
				TurnBestCount = TurnCount;
			}
			ShowMessage("�� �������!");
			laLozung->Text = "���!";
			(LevelKey == false) ? WinCountEasy++ : WinCountHard++;
		}
	}
	int f = Random(4);
	if (f == 0) {
		laLozung->Text = "�� ��!";
	}
	if (f == 1) {
		laLozung->Text = "�������!";
	}
	if (f == 2) {
		laLozung->Text = "��� �������!";
	}
	if (f == 3) {
		laLozung->Text = "������ ���!";
	}
}
//---------------------------------------------------------------------------

void Tfm::Start()
{
    for (int i = 0; i < level; i++) {
		once = true;
		for (int i = 0; i < field; i++) {
			int z = Random(4);
				if (m[i]->ImageIndex == -1 && once == true) {
					if (z == 0 && m[i]->Parent->Tag != 3 && m[i]->Parent->Tag != 6 && m[i]->Parent->Tag != 9) {
					x = m[i]->ImageIndex;
					m[i]->ImageIndex = m[i+1]->ImageIndex;
					m[i+1]->ImageIndex = x;
					once = false;
				}
				if (z == 1 && m[i]->Parent->Tag != 1 && m[i]->Parent->Tag != 4 && m[i]->Parent->Tag != 7) {
					y = m[i]->ImageIndex;
					m[i]->ImageIndex = m[i-1]->ImageIndex;
					m[i-1]->ImageIndex = y;
					once = false;
				}
				if (z == 2 && m[i]->Parent->Tag != 7 && m[i]->Parent->Tag != 8 && m[i]->Parent->Tag != 9) {
					y = m[i]->ImageIndex;
					m[i]->ImageIndex = m[i+3]->ImageIndex;
					m[i+3]->ImageIndex = y;
					once = false;
				}
				if (z == 3 && m[i]->Parent->Tag != 1 && m[i]->Parent->Tag != 2 && m[i]->Parent->Tag != 3) {
					y = m[i]->ImageIndex;
					m[i]->ImageIndex = m[i-3]->ImageIndex;
					m[i-3]->ImageIndex = y;
					once = false;
				}
			}
		}
	}
	laLozung->Text = "������!";
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buEasyClick(TObject *Sender)
{
	Easy();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buHardClick(TObject *Sender)
{
	Hard();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buStartClick(TObject *Sender)
{
	tc->ActiveTab = tiLevel;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buRestartClick(TObject *Sender)
{
	RestartCount++;
	(LevelKey == false) ? Easy() : Hard();
}
//---------------------------------------------------------------------------
void Tfm::Easy()
{
	level = 40;
	Start();
	LevelKey = false;
	tc->ActiveTab = tiPlay;
}
void Tfm::Hard()
{
	level = 80;
	Start();
	LevelKey = true;
	tc->ActiveTab = tiPlay;
}
void __fastcall Tfm::buStatClick(TObject *Sender)
{
	laWinEasy->Text = WinCountEasy;
	laWinHard->Text = WinCountHard;
	laRestart->Text = RestartCount;
	laTurn->Text = TurnBestCount;
	tc->ActiveTab = tiStat;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buBackClick(TObject *Sender)
{
    tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------

