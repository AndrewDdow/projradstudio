//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------

__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->First();
	tc->TabPosition = TTabPosition::None;
	pb->Max = tc->TabCount - 1;
	laCountQ->Visible = false;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buStartClick(TObject *Sender)
{
	xTrue = 0;
	xFalse = 0;
	xValue = 1;
    me->Lines->Clear();
	tc->Next();
	laCountQ->Visible = true;
	laCountQ->Text = Format("%d �� %d", ARRAYOFCONST((xValue, tc->TabCount - 2)));
}
//---------------------------------------------------------------------------

void __fastcall Tfm::ButtonAllClick(TObject *Sender)
{
	UnicodeString x;
	//x = ((TControl *)Sender)->Tag == 1 ? L"�����" : L"�������";
	if (((TControl *)Sender)->Tag == 1) {
	   x = L"�����";
	   xTrue++;
	}
	else{
	   x = L"�������";
	   xFalse++;
	}
	me->Lines->Add(L"������" + tc->ActiveTab->Text + " - " + x);
	// me->Lines->Add(Format(L" %s - %s", ARRAYOFCONST((tc->ActiveTab)))
	xValue++;
	laCountQ->Text = Format("%d �� %d", ARRAYOFCONST((xValue, tc->TabCount - 2)));
	tc->Next();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::ButtonNoClick(TObject *Sender)
{
	UnicodeString x;
	//x = ((TControl *)Sender)->Tag == 1 ? L"�����" : L"�������";
    if (((TControl *)Sender)->Tag == 1) {
	   x = L"�����";
	   xTrue++;
	}
	else{
	   x = L"�������";
	   xFalse++;
	}
	me->Lines->Add(L"������" + tc->ActiveTab->Text + " - " + x);
	// me->Lines->Add(Format(L" %s - %s", ARRAYOFCONST((tc->ActiveTab)))
	tc->Last();
	laTrue->Text = Format("����� = %d", ARRAYOFCONST((xTrue)));
	laFalse->Text = Format("������� = %d", ARRAYOFCONST((xFalse)));
	imCorrect->Visible = (xTrue > xFalse);
	//imWrong->Visible ! imCorrect->Visible;
    imWrong->Visible = (xFalse >= xTrue);
	laCountQ->Visible = false;
}//---------------------------------------------------------------------------

void __fastcall Tfm::buRestartClick(TObject *Sender)
{
	tc->First();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::tcChange(TObject *Sender)
{
	pb->Value = tc->ActiveTab->Index;
}
//---------------------------------------------------------------------------

