//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TLayout *Layout1;
	TLabel *Label1;
	TButton *buAbout;
	TTabControl *tc;
	TTabItem *TabItem1;
	TTabItem *TabItem2;
	TTabItem *TabItem3;
	TTabItem *TabItem4;
	TTabItem *TabItem5;
	TButton *buStart;
	TImage *Image1;
	TLabel *Label2;
	TButton *Button1;
	TButton *Button2;
	TScrollBox *ScrollBox1;
	TButton *Button3;
	TScrollBox *ScrollBox2;
	TLabel *Label3;
	TButton *Button4;
	TButton *Button5;
	TButton *Button6;
	TScrollBox *ScrollBox3;
	TLabel *Label4;
	TButton *Button7;
	TButton *Button8;
	TButton *Button9;
	TButton *buRestart;
	TMemo *me;
	TProgressBar *pb;
	TLabel *laCountQ;
	TRectangle *Rectangle1;
	TLabel *laTrue;
	TLabel *laFalse;
	TImage *imCorrect;
	TImage *imWrong;
	TTabItem *TabItem6;
	TLabel *Label5;
	TGridPanelLayout *GridPanelLayout1;
	TImage *Image2;
	TImage *Image3;
	TImage *Image4;
	TImage *Image5;
	TStyleBook *StyleBook1;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buStartClick(TObject *Sender);
	void __fastcall ButtonAllClick(TObject *Sender);
	void __fastcall ButtonNoClick(TObject *Sender);
	void __fastcall buRestartClick(TObject *Sender);
	void __fastcall tcChange(TObject *Sender);
private:	// User declarations
	int xValue;
	int xTrue;
    int xFalse;
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
