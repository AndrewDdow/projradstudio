//---------------------------------------------------------------------------

#ifndef dmuH
#define dmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.FMXUI.Wait.hpp>
#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.Phys.IBBase.hpp>
//---------------------------------------------------------------------------
class Tdm : public TDataModule
{
__published:	// IDE-managed Components
	TFDConnection *FDConnection1;
	TFDTable *FDTable1;
	TFDQuery *quEmployee;
	TFDGUIxWaitCursor *FDGUIxWaitCursor1;
	TFDPhysFBDriverLink *FDPhysFBDriverLink1;
	TFDQuery *quSalaryHistory;
	TStringField *quEmployeeFIRST_NAME;
	TStringField *quEmployeeLAST_NAME;
	TFMTBCDField *quEmployeeSALARY;
	TSQLTimeStampField *quEmployeeHIRE_DATE;
	TStringField *quEmployeeJOB_COUNTRY;
	TStringField *quEmployeeDEPT_NO;
	TStringField *quEmployeeDEPARTMENT;
	TFMTBCDField *quEmployeeBUDGET;
	TSmallintField *quSalaryHistoryEMP_NO;
	TSQLTimeStampField *quSalaryHistoryCHANGE_DATE;
	TFMTBCDField *quSalaryHistoryOLD_SALARY;
	TFloatField *quSalaryHistoryNEW_SALARY;
	TSmallintField *quEmployeeEMP_NO;
	void __fastcall quEmployeeAfterScroll(TDataSet *DataSet);
	void __fastcall DataModuleCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tdm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tdm *dm;
//---------------------------------------------------------------------------
#endif
