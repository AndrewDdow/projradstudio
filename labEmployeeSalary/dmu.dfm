object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 379
  Width = 606
  object FDConnection1: TFDConnection
    Params.Strings = (
      
        'Database=C:\Program Files\Firebird\Firebird_3_0\examples\empbuil' +
        'd\EMPLOYEE.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'CharacterSet=UTF8'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 288
    Top = 176
  end
  object FDTable1: TFDTable
    IndexFieldNames = 'EMP_NO'
    Connection = FDConnection1
    UpdateOptions.UpdateTableName = 'EMPLOYEE'
    TableName = 'EMPLOYEE'
    Left = 416
    Top = 176
  end
  object quEmployee: TFDQuery
    Active = True
    AfterScroll = quEmployeeAfterScroll
    Connection = FDConnection1
    SQL.Strings = (
      'select'
      '    employee.emp_no,'
      '    employee.first_name,'
      '    employee.last_name,'
      '    employee.salary,'
      '    employee.hire_date,'
      '    employee.job_country,'
      '    employee.dept_no,'
      '    department.department,'
      '    department.budget'
      'from employee'
      
        '   inner join department on (employee.dept_no = department.dept_' +
        'no)'
      'order by employee.first_name, employee.last_name')
    Left = 360
    Top = 176
    object quEmployeeEMP_NO: TSmallintField
      FieldName = 'EMP_NO'
      Origin = 'EMP_NO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quEmployeeFIRST_NAME: TStringField
      FieldName = 'FIRST_NAME'
      Origin = 'FIRST_NAME'
      Required = True
      Size = 15
    end
    object quEmployeeLAST_NAME: TStringField
      FieldName = 'LAST_NAME'
      Origin = 'LAST_NAME'
      Required = True
    end
    object quEmployeeSALARY: TFMTBCDField
      FieldName = 'SALARY'
      Origin = 'SALARY'
      Required = True
      Precision = 18
      Size = 2
    end
    object quEmployeeHIRE_DATE: TSQLTimeStampField
      FieldName = 'HIRE_DATE'
      Origin = 'HIRE_DATE'
      Required = True
    end
    object quEmployeeJOB_COUNTRY: TStringField
      FieldName = 'JOB_COUNTRY'
      Origin = 'JOB_COUNTRY'
      Required = True
      Size = 15
    end
    object quEmployeeDEPT_NO: TStringField
      FieldName = 'DEPT_NO'
      Origin = 'DEPT_NO'
      Required = True
      FixedChar = True
      Size = 3
    end
    object quEmployeeDEPARTMENT: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DEPARTMENT'
      Origin = 'DEPARTMENT'
      ProviderFlags = []
      ReadOnly = True
      Size = 25
    end
    object quEmployeeBUDGET: TFMTBCDField
      AutoGenerateValue = arDefault
      FieldName = 'BUDGET'
      Origin = 'BUDGET'
      ProviderFlags = []
      ReadOnly = True
      Precision = 18
      Size = 2
    end
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 48
    Top = 40
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 48
    Top = 104
  end
  object quSalaryHistory: TFDQuery
    Active = True
    Filtered = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    salary_history.emp_no,'
      '    salary_history.change_date,'
      '    salary_history.old_salary,'
      '    salary_history.new_salary'
      'from salary_history'
      'order by salary_history.emp_no, salary_history.change_date')
    Left = 360
    Top = 232
    object quSalaryHistoryEMP_NO: TSmallintField
      FieldName = 'EMP_NO'
      Origin = 'EMP_NO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quSalaryHistoryCHANGE_DATE: TSQLTimeStampField
      FieldName = 'CHANGE_DATE'
      Origin = 'CHANGE_DATE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quSalaryHistoryOLD_SALARY: TFMTBCDField
      FieldName = 'OLD_SALARY'
      Origin = 'OLD_SALARY'
      Required = True
      Precision = 18
      Size = 2
    end
    object quSalaryHistoryNEW_SALARY: TFloatField
      FieldName = 'NEW_SALARY'
      Origin = 'NEW_SALARY'
    end
  end
end
