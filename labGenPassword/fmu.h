//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.EditBox.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.NumberBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TLayout *Layout1;
	TEdit *edPassword;
	TButton *buPassword;
	TCheckBox *ckLower;
	TCheckBox *ckSpec;
	TCheckBox *ckNumber;
	TCheckBox *ckUpper;
	TNumberBox *edLenght;
	TLabel *laLenght;
	TButton *buAbout;
	TLabel *laCaption;
	TStyleBook *StyleBook1;
	TMemo *me1;
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall buPasswordClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
    int xCount;
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
