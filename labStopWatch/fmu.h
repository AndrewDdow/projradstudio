//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TLabel *laTime;
	TButton *buStart;
	TButton *buStop;
	TTimer *tm;
	TMemo *me;
	TButton *buCircle;
	TButton *buClear;
	TToolBar *tbHeader;
	TButton *buAbout;
	TLabel *Секундомер;
	TToolBar *tbFooter;
	TLabel *Label1;
	void __fastcall buStartClick(TObject *Sender);
	void __fastcall buStopClick(TObject *Sender);
	void __fastcall tmTimer(TObject *Sender);
	void __fastcall buCircleClick(TObject *Sender);
	void __fastcall buClearClick(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
private:	// User declarations
	TDateTime FTimeStart;
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
