//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buStartClick(TObject *Sender)
{
	FTimeStart = Now();
	tm->Enabled = true;
    me->Lines->Clear();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buStopClick(TObject *Sender)
{
	tm->Enabled = false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::tmTimer(TObject *Sender)
{
	UnicodeString x;
	DateTimeToString(x, L"h:nn:ss.zzz", Now() - FTimeStart);
	laTime->Text = x.Delete(x.Length()- 2, 2);
}

void __fastcall TForm1::buCircleClick(TObject *Sender)
{
	me->Lines->Add(laTime->Text);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buClearClick(TObject *Sender)
{
	me->Lines->Clear();
	laTime->Text = "0:00:00";
	tm->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buAboutClick(TObject *Sender)
{
	ShowMessage("���������� �������� ������ 171-333 ����������� ���������������� ������������, ���������� ������. Stopwatch of student group 171-333 Moscow Polytechnic University, Andrey Averyanov.");
}
//---------------------------------------------------------------------------

