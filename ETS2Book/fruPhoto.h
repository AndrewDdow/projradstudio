//---------------------------------------------------------------------------

#ifndef fruPhotoH
#define fruPhotoH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TfrPhoto : public TFrame
{
__published:	// IDE-managed Components
	TImage *im;
private:	// User declarations
public:		// User declarations
	__fastcall TfrPhoto(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrPhoto *frPhoto;
//---------------------------------------------------------------------------
#endif
