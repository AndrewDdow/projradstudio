object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 309
  Width = 530
  object FDConnection1: TFDConnection
    Params.Strings = (
      'User_Name=SYSDBA'
      'Password=masterkey'
      'CharacterSet=UTF8'
      
        'Database=D:\Andrew\'#1059#1095#1077#1073#1072'\'#1052#1086#1073#1080#1083#1100#1085#1072#1103' '#1088#1072#1079#1088#1072#1073#1086#1090#1082#1072'\ETS2Book\ETS2BOOK.' +
        'FDB'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    AfterConnect = FDConnection1AfterConnect
    Left = 400
    Top = 128
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 400
    Top = 176
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 400
    Top = 224
  end
  object quTrucks: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    trucks.id,'
      '    trucks.truck_name,'
      '    trucks.price,'
      '    trucks.fuel_tank_volume,'
      '    trucks.hp,'
      '    trucks.torque,'
      '    trucks.photo_1,'
      '    trucks.photo_2,'
      '    trucks.photo_3,'
      '    trucks.photo_4,'
      '    trucks.photo_5,'
      '    trucks.photo_game'
      'from trucks'
      'order by trucks.id')
    Left = 136
    Top = 144
    object quTrucksID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quTrucksTRUCK_NAME: TWideStringField
      FieldName = 'TRUCK_NAME'
      Origin = 'TRUCK_NAME'
      Size = 15
    end
    object quTrucksPRICE: TIntegerField
      FieldName = 'PRICE'
      Origin = 'PRICE'
    end
    object quTrucksFUEL_TANK_VOLUME: TIntegerField
      FieldName = 'FUEL_TANK_VOLUME'
      Origin = 'FUEL_TANK_VOLUME'
    end
    object quTrucksHP: TIntegerField
      FieldName = 'HP'
      Origin = 'HP'
    end
    object quTrucksTORQUE: TIntegerField
      FieldName = 'TORQUE'
      Origin = 'TORQUE'
    end
    object quTrucksPHOTO_1: TBlobField
      FieldName = 'PHOTO_1'
      Origin = 'PHOTO_1'
    end
    object quTrucksPHOTO_2: TBlobField
      FieldName = 'PHOTO_2'
      Origin = 'PHOTO_2'
    end
    object quTrucksPHOTO_3: TBlobField
      FieldName = 'PHOTO_3'
      Origin = 'PHOTO_3'
    end
    object quTrucksPHOTO_4: TBlobField
      FieldName = 'PHOTO_4'
      Origin = 'PHOTO_4'
    end
    object quTrucksPHOTO_5: TBlobField
      FieldName = 'PHOTO_5'
      Origin = 'PHOTO_5'
    end
    object quTrucksPHOTO_GAME: TBlobField
      FieldName = 'PHOTO_GAME'
      Origin = 'PHOTO_GAME'
    end
  end
  object spFeedback: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'NEW_PROCEDURE'
    Left = 152
    Top = 216
    ParamData = <
      item
        Position = 1
        Name = 'FIO'
        DataType = ftWideString
        ParamType = ptInput
        Size = 70
      end
      item
        Position = 2
        Name = 'PHONE'
        DataType = ftWideString
        ParamType = ptInput
        Size = 15
      end
      item
        Position = 3
        Name = 'EMAIL'
        DataType = ftWideString
        ParamType = ptInput
        Size = 70
      end
      item
        Position = 4
        Name = 'NOTE'
        DataType = ftWideString
        ParamType = ptInput
        Size = 1000
      end>
  end
end
