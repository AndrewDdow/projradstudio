//---------------------------------------------------------------------------

#ifndef fruGalleryH
#define fruGalleryH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
//---------------------------------------------------------------------------
class TfrTrucks : public TFrame
{
__published:	// IDE-managed Components
	TImage *im;
	TLabel *laName;
private:	// User declarations
public:		// User declarations
	__fastcall TfrTrucks(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrTrucks *frTrucks;
//---------------------------------------------------------------------------
#endif
