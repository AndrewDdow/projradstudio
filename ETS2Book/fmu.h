//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.ListBox.hpp>
#include "fruGallery.h"
#include <FMX.Objects.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiGallery;
	TTabItem *tiTruckList;
	TTabItem *tiGame;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buTrucks;
	TButton *buGallery;
	TButton *buCompare;
	TButton *buFeedback;
	TToolBar *ToolBar1;
	TLabel *Label1;
	TButton *Button1;
	TToolBar *ToolBar2;
	TLabel *Label2;
	TLayout *Layout1;
	TGridLayout *glTrucks;
	TToolBar *ToolBar3;
	TLabel *Label3;
	TButton *Button2;
	TTabItem *tiTruckPhoto;
	TScrollBox *ScrollBox2;
	TGridLayout *glPhoto;
	TToolBar *ToolBar5;
	TLabel *laNamePhoto;
	TButton *Button4;
	TTabItem *tiTruckInfo;
	TToolBar *ToolBar6;
	TLabel *laNameTruckInfo;
	TButton *Button5;
	TListView *lvTrucks;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TImage *Photo_1;
	TImage *Photo_2;
	TImage *Photo_3;
	TImage *Photo_4;
	TImage *Photo_5;
	TImage *imTruckInfo;
	TLayout *Layout4;
	TLabel *Label4;
	TLabel *laTorque;
	TLayout *Layout6;
	TLabel *Label6;
	TLabel *laHP;
	TLayout *Layout8;
	TLabel *Label5;
	TLabel *laFuelTankVolume;
	TBindSourceDB *BindSourceDB2;
	TLinkPropertyToField *LinkPropertyToFieldText;
	TLinkPropertyToField *LinkPropertyToFieldBitmap;
	TLinkPropertyToField *LinkPropertyToFieldBitmap2;
	TLinkPropertyToField *LinkPropertyToFieldBitmap3;
	TLinkPropertyToField *LinkPropertyToFieldText2;
	TBindSourceDB *BindSourceDB3;
	TBindSourceDB *BindSourceDB4;
	TLinkPropertyToField *LinkPropertyToFieldText6;
	TLinkPropertyToField *LinkPropertyToFieldText5;
	TLinkPropertyToField *LinkPropertyToFieldText4;
	TLayout *Layout2;
	TLabel *Label7;
	TLabel *laPrice;
	TLinkPropertyToField *LinkPropertyToFieldText3;
	TButton *Button3;
	TTabItem *tiCompareList;
	TToolBar *ToolBar4;
	TLabel *laCompareLastsTruck;
	TButton *Button6;
	TListView *lvCompare;
	TLinkListControlToField *LinkListControlToField2;
	TTabItem *tiCompareInfo;
	TToolBar *ToolBar7;
	TLabel *laCompareName;
	TButton *Button7;
	TLayout *Layout3;
	TLabel *Label10;
	TLabel *laCompareTorque;
	TLayout *Layout5;
	TLabel *Label12;
	TLabel *laCompareHP;
	TButton *Button8;
	TLinkPropertyToField *LinkPropertyToFieldText7;
	TLinkPropertyToField *LinkPropertyToFieldText8;
	TLinkPropertyToField *LinkPropertyToFieldText9;
	TTabItem *tiCompareDouble;
	TToolBar *ToolBar8;
	TLabel *Label11;
	TButton *Button9;
	TGridPanelLayout *GridPanelLayout2;
	TLayout *Layout9;
	TLayout *Layout7;
	TImage *imCompX;
	TLayout *Layout10;
	TLabel *Label9;
	TLabel *laCompPriceX;
	TLayout *Layout11;
	TLabel *Label14;
	TLabel *laCompTorqueX;
	TLayout *Layout12;
	TLabel *Label16;
	TLabel *laCompHPX;
	TLayout *Layout13;
	TLabel *Label18;
	TLabel *laCompFuelX;
	TLayout *Layout14;
	TImage *imCompY;
	TLayout *Layout15;
	TLabel *laCompPriceY;
	TLayout *Layout16;
	TLabel *Label22;
	TLabel *laCompTorqueY;
	TLayout *Layout17;
	TLabel *Label24;
	TLabel *laCompHPY;
	TLayout *Layout18;
	TLabel *Label26;
	TLabel *laCompFuelY;
	TButton *buPlay;
	TLinkPropertyToField *LinkPropertyToFieldBitmap4;
	TLinkPropertyToField *LinkPropertyToFieldBitmap5;
	TLinkPropertyToField *LinkPropertyToFieldBitmap6;
	TToolBar *ToolBar9;
	TLabel *Label13;
	TButton *Button10;
	TImage *imGame;
	TImage *imGameX;
	TImage *imGameY;
	TFloatAnimation *aniX;
	TImage *imCompInfo;
	TFloatAnimation *aniY;
	TLabel *laWin;
	TLayout *Layout19;
	TLabel *laCompNameY;
	TLayout *Layout20;
	TLabel *laCompNameX;
	TLabel *Label20;
	TLayout *Layout21;
	TLayout *layY;
	TLayout *layX;
	TLayout *layFit;
	TLayout *layMost;
	TButton *buPlus;
	TButton *buMinus;
	TTabItem *tiFeedback;
	TToolBar *ToolBar10;
	TLabel *Label8;
	TButton *Button11;
	TScrollBox *ScrollBox1;
	TLabel *Label15;
	TEdit *edFeedbackFIO;
	TLabel *Label17;
	TEdit *edFeedbackPhone;
	TLabel *Label19;
	TLabel *Label21;
	TEdit *edFeedbackEmail;
	TMemo *meFeedbackNote;
	TButton *buSendFeedback;
	void __fastcall buTrucksClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buBackToMenuClick(TObject *Sender);
	void __fastcall buGalleryClick(TObject *Sender);
	void __fastcall fr1imClick(TObject *Sender);
	void __fastcall fr2imClick(TObject *Sender);
	void __fastcall buBackToGalleryClick(TObject *Sender);
	void __fastcall fr3imClick(TObject *Sender);
	void __fastcall buBackToTrucksClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall GalleryTruckOnClick(TObject *Sender);
	void __fastcall lvTrucksItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall BackToTruckInfoClick(TObject *Sender);
	void __fastcall AddToCompareClick(TObject *Sender);
	void __fastcall buCompareClick(TObject *Sender);
	void __fastcall lvCompareItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall BackToCompareListClick(TObject *Sender);
	void __fastcall ClearBackToCompareListClicl(TObject *Sender);
	void __fastcall buPlayClick(TObject *Sender);
	void __fastcall glPhotoResized(TObject *Sender);
	void __fastcall aniYFinish(TObject *Sender);
	void __fastcall aniXFinish(TObject *Sender);
	void __fastcall Layout21Resized(TObject *Sender);
	void __fastcall buPlusClick(TObject *Sender);
	void __fastcall buMinusClick(TObject *Sender);
	void __fastcall buFeedbackClick(TObject *Sender);
	void __fastcall buSendFeedbackClick(TObject *Sender);



private:	// User declarations
	void LoadGallery();
	void LoadPhoto();
	UnicodeString xWin;
	UnicodeString yWin;
	int compare;
	int id1;
	int id2;
	int xPrice;
	int xHP;
	int xTorque;
	int xFuel;
	int xSpeed;
    int yPrice;
	int yHP;
	int yTorque;
	int yFuel;
	int ySpeed;
	bool nav;
    bool photo;
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
