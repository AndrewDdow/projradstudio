//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
#include "fruGallery.h"
#include "fruPhoto.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "fruGallery"
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	photo = false;
	compare = 0;
	tc->ActiveTab = tiMenu;
	tc->TabPosition = TTabPosition::None;
	layMost->Height =layFit->Height - imGame->Height;
	//ShowMessage(layMost->Height);
	layX->Height = imGame->Height / 6;
	layY->Height = imGame->Height / 6;
    fm->Width = imGame->Width;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormShow(TObject *Sender)
{
    LoadGallery();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buTrucksClick(TObject *Sender)
{
	tc->ActiveTab = tiTruckList;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buBackToMenuClick(TObject *Sender)
{
	tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buGalleryClick(TObject *Sender)
{
	tc->ActiveTab = tiGallery;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::fr1imClick(TObject *Sender)
{
	tc->ActiveTab = tiTruckInfo;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::fr2imClick(TObject *Sender)
{
	tc->ActiveTab = tiTruckInfo;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buBackToGalleryClick(TObject *Sender)
{
	tc->ActiveTab = tiGallery;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::fr3imClick(TObject *Sender)
{
	tc->ActiveTab = tiTruckInfo;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buBackToTrucksClick(TObject *Sender)
{
	tc->ActiveTab = tiTruckList;
}
//---------------------------------------------------------------------------
void Tfm::LoadGallery()
{
	dm->quTrucks->First();
	while (!dm->quTrucks->Eof){
		TfrTrucks *x = new TfrTrucks(glTrucks);
		x->Parent = glTrucks;
		x->Align = TAlignLayout::Client;
		x->Name = "frTrucks"+IntToStr(dm->quTrucksID->Value);
		x->laName->Text = dm->quTrucksTRUCK_NAME->Value;
		x->im->Bitmap->Assign(dm->quTrucksPHOTO_1);
		x->Tag = dm->quTrucksID->Value;
		x->OnClick = GalleryTruckOnClick;
		dm->quTrucks->Next();
	}
	glTrucks->RecalcSize();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::GalleryTruckOnClick(TObject *Sender)
{
	int xTruckID = ((TControl*)Sender)->Tag;
	TLocateOptions xLO;
	dm->quTrucks->Locate(dm->quTrucksID->FieldName, xTruckID, xLO);


	tc->ActiveTab = tiTruckPhoto;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::lvTrucksItemClick(TObject * const Sender, TListViewItem * const AItem)
{
	nav = false;
    if (compare < 1) {
		imGameX->Bitmap->Assign(dm->quTrucksPHOTO_GAME);
	}
	else {
		imGameY->Bitmap->Assign(dm->quTrucksPHOTO_GAME);
	}
	tc->ActiveTab = tiTruckInfo;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::BackToTruckInfoClick(TObject *Sender)
{
	tc->ActiveTab = tiTruckInfo;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::AddToCompareClick(TObject *Sender)
{
	if (compare < 1) {
		id1 = dm->quTrucksID->Value;
		xWin = dm->quTrucksTRUCK_NAME->Value;
		xPrice = dm->quTrucksPRICE->Value;
		xHP = dm->quTrucksHP->Value;
		xTorque = dm->quTrucksTORQUE->Value;
		xFuel = dm->quTrucksFUEL_TANK_VOLUME->Value;
		xSpeed = 2000000 / (xHP * xTorque + xFuel);
		//ShowMessage(xSpeed);
		imCompX->Bitmap->Assign(dm->quTrucksPHOTO_1);
        laCompNameX->Text = dm->quTrucksTRUCK_NAME->Value;
		laCompPriceX->Text = dm->quTrucksPRICE->Value;
		laCompHPX->Text = dm->quTrucksHP->Value;
		laCompTorqueX->Text = dm->quTrucksTORQUE->Value;
		laCompFuelX->Text = dm->quTrucksFUEL_TANK_VOLUME->Value;
		compare++;
		laCompareLastsTruck->Text = "Choose 1 more truck";
		if (nav == false){
			tc->ActiveTab = tiTruckList;
		}
		else {
			tc->ActiveTab = tiCompareList;
		}
	}
	else {
		id2 = dm->quTrucksID->Value;
		if (id1 != id2) {
			yWin = dm->quTrucksTRUCK_NAME->Value;
			yPrice = dm->quTrucksPRICE->Value;
			yHP = dm->quTrucksHP->Value;
			yTorque = dm->quTrucksTORQUE->Value;
			yFuel = dm->quTrucksFUEL_TANK_VOLUME->Value;
			ySpeed = 2000000 / (yHP * yTorque + yFuel);
			//ShowMessage(ySpeed);
			imCompY->Bitmap->Assign(dm->quTrucksPHOTO_1);
			laCompNameY->Text = dm->quTrucksTRUCK_NAME->Value;
			laCompPriceY->Text = dm->quTrucksPRICE->Value;
			laCompHPY->Text = dm->quTrucksHP->Value;
			laCompTorqueY->Text = dm->quTrucksTORQUE->Value;
			laCompFuelY->Text = dm->quTrucksFUEL_TANK_VOLUME->Value;
			//colors
//			if (xPrice < yPrice) {
//				laCompNameX->TextSettings->FontColor = RGB(255,91,91);
//				laCompNameY->TextSettings->FontColor = RGB(34,255,18);
//			}
//			else {
//				laCompNameY->TextSettings->FontColor = (claGreen);
//				laCompNameX->TextSettings->FontColor = (claRed);
//			}
			//move
			tc->ActiveTab = tiCompareDouble;
		}
		else{
			id2 = 0;
			ShowMessage("Choose another truck");
			if (nav == false){
				tc->ActiveTab = tiTruckList;
			}
			else {
				tc->ActiveTab = tiCompareList;
			}
		}
	}

}
//---------------------------------------------------------------------------

void __fastcall Tfm::buCompareClick(TObject *Sender)
{
	tc->ActiveTab = tiCompareList;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::lvCompareItemClick(TObject * const Sender, TListViewItem * const AItem)
{
	nav = true;
	if (compare < 1) {
		imGameX->Bitmap->Assign(dm->quTrucksPHOTO_GAME);
	}
	else {
		imGameY->Bitmap->Assign(dm->quTrucksPHOTO_GAME);
	}
	imCompInfo->Bitmap->Assign(dm->quTrucksPHOTO_GAME);
	tc->ActiveTab = tiCompareInfo;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::BackToCompareListClick(TObject *Sender)
{
	tc->ActiveTab = tiCompareList;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::ClearBackToCompareListClicl(TObject *Sender)
{
	xHP = 0;
	xTorque = 0;
	xFuel = 0;
	xSpeed = 0;
	xWin = "";
	yHP = 0;
	yTorque = 0;
	yFuel = 0;
	ySpeed = 0;
    yWin = "";
	compare = 0;
	id1 = 0;
	id2 = 0;
	aniX->StartValue = 0;
	aniY->StartValue = 0;
	laCompareLastsTruck->Text = "Choose 2 truck";
	laWin->Text = "";
	imGameX->Visible = false;
	imGameY->Visible = false;
	tc->ActiveTab = tiCompareList;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buPlayClick(TObject *Sender)
{
	tc->ActiveTab = tiGame;
	aniX->StopValue = fm->Width;
	aniY->StopValue = fm->Width;
	if (xSpeed == ySpeed) {
		aniX->Duration = 3;
		aniY->Duration = 3;
	}
	else {
		aniX->Duration = xSpeed;
		aniY->Duration = ySpeed;
	}
	imGameX->Visible = true;
	imGameY->Visible = true;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::glPhotoResized(TObject *Sender)
{
	if (fm->Width >= Photo_1->Width * 2 && fm->Width < Photo_1->Width * 3) {
		glPhoto->Height = Photo_1->Height * 3;
	}
	if (fm->Width >= Photo_1->Width * 3) {
		glPhoto->Height = Photo_1->Height * 2;
	}
	if (fm->Width < Photo_1->Width * 2) {
		glPhoto->Height = Photo_1->Height * 5;
	}
}
//---------------------------------------------------------------------------

void __fastcall Tfm::aniYFinish(TObject *Sender)
{
	if (xSpeed < ySpeed) {
		laWin->Text = (xWin + " " + L" Win");
	}
	if (xSpeed > ySpeed) {
		laWin->Text = (yWin + " " + L" Win");
	}
}
//---------------------------------------------------------------------------

void __fastcall Tfm::aniXFinish(TObject *Sender)
{
    if (xSpeed < ySpeed) {
		laWin->Text = (xWin + " " + L" Win");
	}
	if (xSpeed > ySpeed) {
		laWin->Text = (yWin + " " + L" Win");
	}
	if (xSpeed == ySpeed) {
		laWin->Text = "Draw";
	}
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Layout21Resized(TObject *Sender)
{
	layMost->Height = layFit->Height - imGame->Height;
	layX->Height = imGame->Height / 6;
	layY->Height = imGame->Height / 6;
}
//---------------------------------------------------------------------------


void __fastcall Tfm::buPlusClick(TObject *Sender)
{
	glPhoto->ItemHeight += 17;
	glPhoto->ItemWidth += 30;
	glPhotoResized(glPhoto);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buMinusClick(TObject *Sender)
{
	glPhoto->ItemHeight -= 17;
	glPhoto->ItemWidth -= 30;
	glPhotoResized(glPhoto);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buFeedbackClick(TObject *Sender)
{
	edFeedbackFIO->Text = "";
	edFeedbackPhone->Text = "";
	edFeedbackEmail->Text = "";
	meFeedbackNote->Lines->Clear();
	tc->ActiveTab = tiFeedback;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buSendFeedbackClick(TObject *Sender)
{
	dm->Feedback(
	edFeedbackFIO->Text
	,edFeedbackPhone->Text
	,edFeedbackEmail->Text
	,meFeedbackNote->Text
	);
	ShowMessage("Feedback was sended!");
	tc->GotoVisibleTab(tiMenu->Index);
}
//---------------------------------------------------------------------------

