//---------------------------------------------------------------------------


#pragma hdrstop

#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tdm::DataModuleCreate(TObject *Sender)
{
	FDConnection1->Params->Values["DataBase"] = "..\\..\\ETS2BOOK.FDB";
    FDConnection1->Connected = true;
}
//---------------------------------------------------------------------------
void __fastcall Tdm::FDConnection1AfterConnect(TObject *Sender)
{
    quTrucks->Open();
}
//---------------------------------------------------------------------------
void Tdm::Feedback(UnicodeString aFIO, UnicodeString aPhone,
	UnicodeString aEmail, UnicodeString aNote)
{
	spFeedback->ParamByName("FIO")->Value=aFIO;
	spFeedback->ParamByName("PHONE")->Value=aPhone;
	spFeedback->ParamByName("EMAIL")->Value=aEmail;
	spFeedback->ParamByName("NOTE")->Value=aNote;
	spFeedback->ExecProc();
}

