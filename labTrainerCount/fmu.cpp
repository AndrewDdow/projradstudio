//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
void Tfm::DoReset()
{
	FCountCorrect = 0;
	FCountWrong = 0;
	(FLevel == true) ? DoContinueEasy() : DoContinueHardChoose();
}
void Tfm::DoContinueEasy()
{
	laCorrect->Text = Format(L"����� = %d", ARRAYOFCONST((FCountCorrect)));
	laWrong->Text = Format(L"������� = %d", ARRAYOFCONST((FCountWrong)));
	//
	int xValue1 = Random(20);
	int xValue2 = Random(20);
	int xSign = (Random(2)==1) ? 1 : -1;
	int xResult = xValue1 + xValue2;
	int xResultNew = (Random(2) == 1) ? xResult : xResult + (Random(7)*xSign);
	//
	FAnswerCorrect = (xResult == xResultNew);
	laCode->Text = Format("%d + %d = %d", ARRAYOFCONST((xValue1, xValue2, xResultNew)));
}
void Tfm::DoContinueHardPlus()
{
	laCorrect->Text = Format(L"����� = %d", ARRAYOFCONST((FCountCorrect)));
	laWrong->Text = Format(L"������� = %d", ARRAYOFCONST((FCountWrong)));
	//
	int xValue1 = Random(50)*3;
	int xValue2 = Random(50)*3;
	int xSign = (Random(2)==1) ? 1 : -1;
	int xResult = xValue1 + xValue2;
	int xResultNew = (Random(2) == 1) ? xResult : xResult + (Random(3)*xSign);
	//
	FAnswerCorrect = (xResult == xResultNew);
	laCode->Text = Format("%d + %d = %d", ARRAYOFCONST((xValue1, xValue2, xResultNew)));
}
void Tfm::DoContinueHardMinus()
{
	laCorrect->Text = Format(L"����� = %d", ARRAYOFCONST((FCountCorrect)));
	laWrong->Text = Format(L"������� = %d", ARRAYOFCONST((FCountWrong)));
	//
	int xValue1 = Random(50)*3;
	int xValue2 = Random(50)*3;
	int xSign = (Random(2)==1) ? 1 : -1;
	int xResult = xValue1 - xValue2;
	int xResultNew = (Random(2) == 1) ? xResult : xResult + (Random(3)*xSign);
	//
	FAnswerCorrect = (xResult == xResultNew);
	laCode->Text = Format("%d - %d = %d", ARRAYOFCONST((xValue1, xValue2, xResultNew)));
}
void Tfm::DoAnswer(bool aValue)
{
	(aValue == FAnswerCorrect) ? FCountCorrect++ : FCountWrong++;
	(aValue == FAnswerCorrect) ? ShowHappy() : ShowSad();
	(FLevel == true) ? DoContinueEasy() : DoContinueHardChoose();
}

//---------------------------------------------------------------------------
void __fastcall Tfm::buRestartClick(TObject *Sender)
{
	DoReset();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buYesClick(TObject *Sender)
{
	DoAnswer(True);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buNoClick(TObject *Sender)
{
	DoAnswer(False);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
    FLevel = true;
	DoReset();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buAboutClick(TObject *Sender)
{
	ShowMessage(L"��������� ��� ���������� ������� ����� �������� ������ 171-333 ����������� ���������������� ������������, ���������� ������.");
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buEasyClick(TObject *Sender)
{
	DoLevel(True);
    DoReset();
	laLevel->Text = (L"˸���� �������");
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buHardClick(TObject *Sender)
{
	DoLevel(False);
	DoReset();
	laLevel->Text = (L"������ �������");
}
//---------------------------------------------------------------------------

void Tfm::DoLevel(bool aLevel)
{
	FLevel = aLevel;
	(aLevel == true) ? DoContinueEasy() : DoContinueHardChoose();
}
//---------------------------------------------------------------------------
void Tfm::DoContinueHardChoose()
{
	(Random(2)==1) ? DoContinueHardPlus() : DoContinueHardMinus();
}
void Tfm::ShowHappy()
{
	imHappy -> Visible = true;
	FloatAnimation1->Start();

}
//---------------------------------------------------------------------------

void Tfm::ShowSad()
{
	imSad -> Visible = true;
	FloatAnimation2->Start();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FloatAnimation1Finish(TObject *Sender)
{
	imHappy -> Visible = false;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FloatAnimation2Finish(TObject *Sender)
{
    imSad -> Visible = false;
}
//---------------------------------------------------------------------------

