//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Ani.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *tbHeader;
	TButton *buRestart;
	TButton *buAbout;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buYes;
	TButton *buNo;
	TGridPanelLayout *GridPanelLayout2;
	TRectangle *Rectangle1;
	TLabel *laCorrect;
	TRectangle *Rectangle2;
	TLabel *laWrong;
	TLabel *laQuestion;
	TLabel *laCode;
	TButton *buEasy;
	TButton *buHard;
	TLabel *laLevel;
	TImage *imSad;
	TImage *imHappy;
	TFloatAnimation *FloatAnimation1;
	TFloatAnimation *FloatAnimation2;
	TLabel *laName;
	void __fastcall buRestartClick(TObject *Sender);
	void __fastcall buYesClick(TObject *Sender);
	void __fastcall buNoClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall buEasyClick(TObject *Sender);
	void __fastcall buHardClick(TObject *Sender);
	void __fastcall FloatAnimation1Finish(TObject *Sender);
	void __fastcall FloatAnimation2Finish(TObject *Sender);
private:	// User declarations
	int FCountCorrect;
	int FCountWrong;
	bool FAnswerCorrect;
	bool FLevel;
	void DoReset();
	void DoContinueEasy();
	void DoContinueHardChoose();
	void DoContinueHardPlus();
	void DoContinueHardMinus();
	void ShowSad();
	void ShowHappy();
	void DoAnswer(bool aValue);
	void DoLevel(bool aLevel);
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
