//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include <System.IOUtils.hpp>
#include <IdAttachmentFile.hpp>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buSendClick(TObject *Sender)
{
	IdSMTP->Host = "smtp.mail.ru";
	IdSMTP->Port = 587;
	IdSMTP->Username = "andre041099@mail.ru";
	IdSMTP->Password = "";
	IdSMTP->UseTLS = utUseExplicitTLS;
	IdSMTP->ReadTimeout = 15000;
	IdSMTP->Connect();
	if (IdSMTP->Connected() == true) {
			TIdMessage *x = new TIdMessage();
			try {
			//  x->From->Name = Trim(___);
				x->From->Address = IdSMTP->Username;
				x->Recipients->Add()->Address = Trim(edTo->Text);
				x->Subject = Trim(edSubject->Text);
				x->Body->Assign(meBody->Lines);
				x->CharSet = "Windows-1251";
				if (ckIncludeAtt->IsChecked) {
					UnicodeString xFileName = Ioutils::TPath::ChangeExtension(
						Ioutils::TPath::GetTempFileName(), ".txt");
					meBody->Lines->SaveToFile(xFileName, TEncoding::UTF8);
					new TIdAttachmentFile(x->MessageParts, xFileName);
				}
				IdSMTP->Send(x);
				ShowMessage(L"������ ����������");
			}
			__finally {
				delete x;
				IdSMTP->Disconnect();
			}
	}

}
//---------------------------------------------------------------------------
