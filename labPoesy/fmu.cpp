//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
inline int Low(const UnicodeString &)
{
#ifdef _DELPHI_STRING_ONE_BASED
	return 1;
#else
	return 0;
#endif
}
//---------------------------------------------------------------------------
inline int High(const UnicodeString &S)
{
#ifdef _DELPHI_STRING_ONE_BASED
	return S.Length();
#else
	return S.Length() - 1;
#endif
}
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->First();
	//
	UnicodeString x;
	//����� ������
	for (int i = 0; i < meFull->Lines->Count; i++) {
		x = meFull->Lines->Strings[i];
		if (i % 2 == 1) {
			for (int j = Low(x); j <= High(x); j++) {
				if (x[j] != ' ')
					x[j] = 'x';
			}
		}
		me1->Lines->Add(x);
	}
	//������ ����� � ������
	bool xFlag;
	for (int i = 0; i < meFull->Lines->Count; i++) {
		x = meFull->Lines->Strings[i];
		xFlag = false;
		for (int j = Low(x); j <= High(x); j++) {
			if ((xFlag) && (x[j] !=' '))
				x[j] = 'x';
			if ((!xFlag) && (x[j] ==' '))
				xFlag = true;
		}
		me2->Lines->Add(x);
	}
	//������ ����� � ������
	bool yFlag;
	for (int i = 0; i < meFull->Lines->Count; i++) {
		x = meFull->Lines->Strings[i];
		yFlag = false;
		for (int y = Low(x); y <= High(x); y++) {
			if ((yFlag) && (x[y] !=' '))
				x[y] = 'x';
				yFlag = true;
			if ((!yFlag) && (x[y] ==' '))
				yFlag = true;
		}
		me3->Lines->Add(x);
	}
	//������ ����� � �����
	bool zFlag;
	bool dFlag;
	for (int i = 0; i < meFull->Lines->Count; i++) {
		x = meFull->Lines->Strings[i];
		zFlag = false;
		dFlag = false;
		for (int j = Low(x); j <= High(x); j++) {
			if (x[j] ==',')
				x[j] =',';
			if ((!dFlag) &&(zFlag) && (x[j] !=' '))
				x[j] = 'x';
			if ((!zFlag) && (x[j] !=' '))
				zFlag = true;
                dFlag = false;
			//if ((dFlag) && (x[j] !=' '))
			//	dFlag = false;
			if ((!dFlag) &&(zFlag) && x[j] ==' '){
				zFlag = false;
				dFlag = true;
			}
		}
		me4->Lines->Add(x);
	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buSizeOutClick(TObject *Sender)
{
	meFull->TextSettings->Font->Size -= 2;
	me1->TextSettings->Font->Size -= 2;
	me2->TextSettings->Font->Size -= 2;
	me3->TextSettings->Font->Size -= 2;
	me4->TextSettings->Font->Size -= 2;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buSizeInClick(TObject *Sender)
{
	meFull->TextSettings->Font->Size += 2;
	me1->TextSettings->Font->Size += 2;
	me2->TextSettings->Font->Size += 2;
	me3->TextSettings->Font->Size += 2;
	me4->TextSettings->Font->Size += 2;
}
//---------------------------------------------------------------------------
