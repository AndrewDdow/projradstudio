//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.ImgList.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Colors.hpp>
#include <FMX.Edit.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buNewImage;
	TButton *buNewRect;
	TButton *buClear;
	TButton *buAbout;
	TGlyph *Glyph1;
	TSelection *Selection1;
	TLayout *ly;
	TSelection *Selection2;
	TGlyph *Glyph2;
	TSelection *Selection3;
	TRectangle *Rectangle1;
	TToolBar *tbOptions;
	TButton *buBringToFront;
	TButton *buSendToBack;
	TTrackBar *trRotation;
	TButton *buDel;
	TToolBar *tbImage;
	TButton *buImageFront;
	TButton *buImageNext;
	TButton *buImageSelect;
	TButton *buImageRND;
	TToolBar *tbRect;
	TTrackBar *trRectRadius;
	TSelection *Selection4;
	TToolBar *tbText;
	TComboColorBox *ComboColorBoxRect;
	TComboColorBox *ComboColorBoxText;
	TEdit *editText;
	TButton *buNewText;
	TButton *buSizeMinus;
	TButton *buSizePlus;
	TButton *buStock;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall SelectionAllMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);
	void __fastcall buBringToFrontClick(TObject *Sender);
	void __fastcall buSendToBackClick(TObject *Sender);
	void __fastcall trRotationChange(TObject *Sender);
	void __fastcall buDelClick(TObject *Sender);
	void __fastcall buImageFrontClick(TObject *Sender);
	void __fastcall buImageNextClick(TObject *Sender);
	void __fastcall buImageSelectClick(TObject *Sender);
	void __fastcall buImageRNDClick(TObject *Sender);
	void __fastcall ComboColorBoxRectChange(TObject *Sender);
	void __fastcall trRectRadiusChange(TObject *Sender);
	void __fastcall buNewImageClick(TObject *Sender);
	void __fastcall buNewRectClick(TObject *Sender);
	void __fastcall buClearClick(TObject *Sender);
	void __fastcall ComboColorBoxTextChange(TObject *Sender);
	void __fastcall editTextChange(TObject *Sender);
	void __fastcall buNewTextClick(TObject *Sender);
	void __fastcall buSizeMinusClick(TObject *Sender);
	void __fastcall buSizePlusClick(TObject *Sender);
	void __fastcall buStockClick(TObject *Sender);
private:	// User declarations
    bool z;
	TSelection *FSel;
	void SelectionAll(TObject *Sender);
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
